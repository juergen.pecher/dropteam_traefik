#!/bin/bash -v

#
# Use this file to start the docker_devbox on a local developer machine
#

# 1. Check if env.local.example file exists
if [ -e env.local.example ]; then
   cp env.local.example .env
else
   echo "Please set up a env.local.example file before starting your environment."
   exit 1
fi

# 2. Check if .env file was created
if [ -e .env ]; then
   echo "The env.local.example file was successfully copied to the .env file ."
else
   echo "An error occured on the .env file creation."
   exit 1
fi

# 3. Create docker network
docker network create web

# 3. Start the services
docker-compose -f docker-compose.yml up -d